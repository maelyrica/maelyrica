#include "TekstowoDownloader.h"
#include <QTextDocument>

QString TekstowoDownloader::toProviderCode(const QString &artist, const QString &track) const
{
    const QString rooturl("http://tekstowo.pl/piosenka,%1,%2.html");
    const QString artist_www( artist.trimmed().toLower().replace(' ', '_').replace('(', '_').replace(')', '_') );
    const QString track_www( track.trimmed().toLower().replace(' ', '_').replace('(', '_').replace(')', '_') );

    return rooturl.arg(artist_www, track_www);
}

bool TekstowoDownloader::parse()
{
    const QString startMarker("<h2>Tekst piosenki:</h2><br />"), endMarker("<p>&nbsp;</p>");
    int startpos = lyrics_qstr.indexOf(startMarker) + startMarker.length();
    if (startpos < 0) return false;

    lyrics_qstr = QTextDocument(lyrics_qstr.remove(0, startpos ).mid(0, lyrics_qstr.indexOf(endMarker)-3).trimmed()).toPlainText();

    return true;

    /*istringstream temp(buff.str());
    buff.str();
    string line;
    QString line_QStr, lyrics_QStr;
    const QString startmarker("<h2>Tekst piosenki:</h2><br />"), endmarker("<p>&nbsp;</p>"); // TODO text
    while (temp.good())
    {
        getline(temp, line);
        if (line_QStr = QString::fromUtf8(line.c_str()), line_QStr.contains(startmarker)  )
        {
            getline(temp, line);
            if ( ! ( line_QStr = QString::fromUtf8(line.c_str())).isEmpty()  )
                lyrics_QStr += line_QStr;
        }
    }

    if (!temp.good()) return false;

    while (temp.good())
    {
        getline(temp, line);
        if ( line_QStr = QString::fromUtf8(line.c_str()), line_QStr.contains(endmarker)) break;
        else lyrics_QStr += line_QStr;
    }

    if (!temp.good()) return false;

    QTextDocument lyricsHtml;
    lyricsHtml.setHtml(lyrics_QStr);
    lyrics_qstr = lyricsHtml.toPlainText();
    return true;*/
}
