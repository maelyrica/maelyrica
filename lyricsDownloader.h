/* File: main.cpp
 *
 * This file is part of MaeLyrica.
 *
 * Copyright (C) 2012 Marcin Mielniczuk.
 *
 * MaeLyrica is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 * MaeLyrica is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with MaeLyrica.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LYRICSDOWNLOADER_H
#define LYRICSDOWNLOADER_H

#include <sstream>
#include <curl/curl.h>
#include <QRegExp>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>

#include "settings.h"

using namespace std;

#define DEBUG 1

class LyricsDownloaderManager;

class lyricsDownloader : public QObject
{
    Q_OBJECT
    friend class LyricsDownloaderManager;
public:

    virtual short perform(const QString & a, const QString & t);
    /* Do all the activities needed for the lyrics to be shown to the user
     * Return values:
     * -1 - no internet
     * 0  - OK
     * 1  - Error downloading
     * 2  - Error parsing
     */

protected:
    //lyricsDownloader(const string & a, const string & t ) : artist(a), track(t)

    // settings object
    // functions
    virtual QNetworkReply::NetworkError download(const QString& a, const QString& t) ;
    virtual QString toProviderCode(const QString& artist, const QString& track) const = 0;
    virtual bool parse() = 0;
    // overloaded for QML functions
    // data
    //static const QString rooturl; // url for this provider
    // helper stuff for curl
    // static size_t write_data_to_var(char *ptr, size_t size, size_t nmemb, void *userdata);
    ostringstream buff;
    QString lyrics_qstr; // lyrics are stored here until LyricsDownloaderManager gets them
};

///////////////////////////////////////////////////////

class LyricsDownloaderManager : public QObject
{
    Q_OBJECT
    Q_ENUMS(LyricsDownloaderClass)
public:
    Q_PROPERTY(QString lyrics READ getLyrics WRITE setLyrics NOTIFY lyricsChanged)
    Q_PROPERTY(QString savedLyrics READ getSavedLyrics WRITE setSavedLyrics NOTIFY savedLyricsChanged)
    Q_PROPERTY(bool theseLyricsSaved READ getTheseLyricsSaved NOTIFY theseLyricsSavedSignal)

    enum LyricsDownloaderClass
    {
        AZLyricsDownloaderClass = 0,
        LyricWikiDownloaderClass = 1,
        ChartLyricsDownloaderClass = 2,
        LyricsManiaDownloaderClass = 3,
        TekstowoDownloaderClass = 4

    } currentClass;

    LyricsDownloaderManager(LyricsDownloaderClass lyricsDownloaderName = AZLyricsDownloaderClass );
    ~LyricsDownloaderManager();

    Q_INVOKABLE void changeLyricsDownloader(LyricsDownloaderClass lyricsDownloaderName);

    // functions working on data for any downloader
    Q_INVOKABLE short perform();
    Q_INVOKABLE inline void setData(const QString & a, const QString & t) // for QStrings
        { artist = a; track = t; emit dataChanged(); }
    Q_INVOKABLE inline void setData(const QString & str) {setData(str.split('-')); }
    Q_INVOKABLE inline bool anyFieldEmpty() // check whether everything is filled
        { if ( artist.isEmpty() || track.isEmpty() ) return true; else return false; }

    Q_INVOKABLE inline bool lyricsExistMaeLyrica() {return progSettings.contains(savedLyricsKey);}
    Q_INVOKABLE void saveLyricsMaeLyrica();
    Q_INVOKABLE inline void deleteLyricsMaeLyrica() {progSettings.remove(savedLyricsKey);}
protected:
    // internal functions
    lyricsDownloader * newDownloader(LyricsDownloaderClass & name);
    // internal data
    lyricsDownloader * downloader;
    QString artist, track;
    QString lyrics;
    static Settings progSettings;

    // additional internal data
    QString savedLyricsKey;
    static const QString recentSearchesUrl;
    QString cleanedTitle;
    QString cleanQString(QString qstr); // clean for saving, as in legacy OMP lyrics

    // versions of public functions for internal usage only
    inline void setData(const QStringList& list) // for a list "artist - track"
        { artist = list[0]; track = list[1]; emit dataChanged(); }

private:
    // Q _PROPERTY functions
    inline void setLyrics(QString newlyrics) {lyrics = newlyrics;}
    inline void setSavedLyrics(QString newlyrics) { progSettings.setValue(savedLyricsKey, newlyrics); emit savedLyricsChanged(newlyrics); }
    inline QString getLyrics() const {return lyrics;}
    inline QString getSavedLyrics() const {return progSettings.value(savedLyricsKey).value<QString>();}
    inline bool getTheseLyricsSaved() const {return progSettings.contains(savedLyricsKey);}

signals: // for Q_PROPERTY
    void dataChanged();
    void lyricsChanged(QString);
    void savedLyricsChanged(QString);
    void theseLyricsSavedSignal(QString);

    void noInternet();

protected slots:
    void findSavedLyricsKey() {savedLyricsKey = "lyrics/" + cleanedTitle;}
    void cleanTitle();
};

#endif // LYRICSDOWNLOADER_H
