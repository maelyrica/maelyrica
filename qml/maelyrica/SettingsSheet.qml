import QtQuick 1.1
import com.nokia.meego 1.0
import com.nokia.extras 1.0


Sheet
{
    id: settingsSheet

    acceptButtonText: "OK"
    rejectButtonText: "Defaults"
    onRejected:
    {
        restoreDefaultsDialog.open()
    }

    content: Rectangle
    {
        anchors.fill: parent
        width: parent.width
        height: parent.height
        color: "#E0E1E2"
        Column
        {
            spacing: 50
            width: parent.width
            Item {width: 1} // add some space :)
            Button
            {
                width: parent.width
                text: "Clear recent searches"
                //enabled: programSettings.anyRecentSearches
                onClicked:
                {
                    if (programSettings.anyRecentSearches)
                    {
                        removeRecentSearchesDialog.open()
                    }
                    else
                    {
                        noRecentSearchesBanner.show()
                    }
                }
            }
            Row
            {
                width: parent.width
                spacing: 20
                RowSpacer
                {
                    id: spacerautoscan
                    width: 10
                }

                Label
                {
                    id: autoScanSavesLabel
                    text: "Scan for saved lyrics"
                    width: parent.width - autoScanSavesSwitch.width * 2 - spacerautoscan.width
                }
                Switch
                {
                    id: autoScanSavesSwitch
                    checked: programSettings.autoScanSaves
                }
            }
            Button
            {
                width: parent.width
                text: "Remove all saved lyrics"
                onClicked: removeSavedLyricsDialog.open()
            }
        }
    }

    QueryDialog
    {
        id: restoreDefaultsDialog
        acceptButtonText: "Yes"
        rejectButtonText: "No"
        titleText: "Restore default settings"
        message: "Are you sure you want to restore the default settings?"
        onAccepted:
        {
            programSettings.restoreDefaults()
            settingsRestoredBanner.show()
        }
    }
    QueryDialog
    {
        id: removeRecentSearchesDialog
        acceptButtonText: "Yes"
        rejectButtonText: "No"
        titleText: "Remove recent searches"
        message: "Are you sure you want to remove all the recent searches?"
        onAccepted:
        {
            programSettings.setRecentSearches() // clear them
            recentSeachesClearedBanner.show()
        }
    }
    QueryDialog
    {
        id: removeSavedLyricsDialog
        acceptButtonText: "Yes"
        rejectButtonText: "No"
        titleText: "Remove saved lyrics"
        message: "Are you sure you want to remove all the saved lyrics?"
        onAccepted:
        {
            programSettings.clearSavedLyrics()
            savedLyricsClearedBanner.show()
        }
    }

    InfoBanner
    {
        id: savedLyricsClearedBanner
        timerShowTime: 1800
        text: "All saved lyrics cleared"
    }
    InfoBanner
    {
        id: recentSeachesClearedBanner
        timerShowTime: 1800
        text: "Recent searches cleared"
    }
    InfoBanner
    {
        id: noRecentSearchesBanner
        timerShowTime: 1800
        text: "No recent searches saved"
    }
    InfoBanner
    {
        id: settingsRestoredBanner
        timerShowTime: 1800
        text: "Settings restored"
    }
}
