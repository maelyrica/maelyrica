import QtQuick 1.1
import com.nokia.meego 1.0
import QtMobility.gallery 1.1

Page
{
    id: selectTrackPage
    tools: replacebacktoolbar
    Loader
    {
        id: replaceBackLoader
    }

    ListView
    {
        id: galleryView
        anchors.fill: parent
        clip: true
        model: trackModel
        delegate: Item
        {
            height: 60
            width: parent.width
            Button
            {
                height: 50
                width: parent.width - 20
                text: (title != "") ? title : "(unknown title)"
                //text: selectedartist
                font.pixelSize: 25

                anchors
                {
                    verticalCenter: parent.verticalCenter
                    left: parent.left
                    leftMargin: 10
                }
                onClicked:
                {
                    selecttrack.text = selectedartist + " - " + title
                    downloader.setData(selectedartist, title)
                    pageStack.pop();
                    searchString = selecttrack.text
                }
            }
            Image
            {
                source: "image://theme/icon-m-invitation-accept"
                anchors
                {
                    right: parent.right
                    rightMargin: 15
                    verticalCenter: parent.verticalCenter
                }
            }

        }
        header: MaeLyricaHeader {headertext: "Select track"}
    }

    DocumentGalleryModel
    {
        id: trackModel
        rootType: DocumentGallery.Audio
        properties: ["title", "artist"]
        sortProperties: ["+title"]
        filter: GalleryEqualsFilter
        {
            property: "artist"
            value: selectedartist
        }
        // In Fremantle we cannot filter by albums due to bug in Qt Mobility and need to show tracks only
    }

    ToolBarLayout
    {
        id: replacebacktoolbar
        ToolIcon
        {
            iconId: "toolbar-back"
            onClicked:
            {
                replaceBackLoader.source = "SelectArtistPage.qml"
                pageStack.replace(replaceBackLoader.item)
            }
        }
    }
}
