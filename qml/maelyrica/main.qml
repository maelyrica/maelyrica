import QtQuick 1.1
import com.nokia.meego 1.0
import com.nokia.extras 1.0
import MaeLyrica 1.0

//property color fontcolor: "white"

PageStackWindow
{
    id: pagestackwindow
    property int screenwidth: (screen.currentOrientation == Screen.Landscape) ? screen.displayWidth : screen.displayHeight // to ensure
    property int screenheight: (screen.currentOrientation == Screen.Landscape) ? screen.displayHeight : screen.displayWidth // to ensure

    visible: true
    MainPage
    {
        id: mainview
    }
    initialPage: mainview

    LyricsDownloaderManager { id: downloader }

    Settings { id: programSettings }

    // downloaders
}

