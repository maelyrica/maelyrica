import QtQuick 1.1
import com.nokia.meego 1.0
import com.nokia.extras 1.0

Page
{
    id: showLyricsPage
    tools: showLyricsToolbar

    property int textmargins: 10
    Flickable
    {
        width: parent.width
        height: parent.height
        contentHeight:  lyricsview.paintedHeight + titleHeader.height + 2 * textmargins
        //contentWidth: lyricsview.paintedWidth
        clip: true

        MaeLyricaHeader
        {
            id: titleHeader
            headertext: searchString
            anchors.top:  parent.top
        }
        TextEdit
        {
            anchors
            {
                left: parent.left
                right: parent.right
                top: titleHeader.bottom
                bottom: parent.bottom
                margins: textmargins
            }

            id: lyricsview
            readOnly: true
            text: (readingSaved) ? downloader.savedLyrics : downloader.lyrics
            font.pointSize: 18
        }
    }

    ToolBarLayout
    {
        id: showLyricsToolbar
        ToolIcon
        {
            iconId: "toolbar-back"
            onClicked:
            {
                pageStack.pop()
                lyricsview.readOnly = true
            }
        }
        ToolIcon
        {
            iconId: "toolbar-directory-move-to"
            onClicked: saveImportMenu.open()
        }
        ToolIcon
        {
            id: deleteIcon
            iconId: "toolbar-delete"
            visible: (readingSaved) ? true : false
            onClicked: deleteLyricsMaeLyricaDialog.open()
        }

        ToolIcon
        {
            iconId: "toolbar-edit"
            onClicked:
            {
                lyricsview.readOnly = !lyricsview.readOnly
                editModeChangedBanner.show()
            }
        }
    }

    // Info about disabling/enabling edit mode

    InfoBanner
    {
        id: editModeChangedBanner
        text: "Lyrics editing " +  ( lyricsview.readOnly ? "disabled" : "enabled")
        timerShowTime: 1000
    }

    Menu
    {
        id: saveImportMenu
        visualParent: pageStack
        MenuLayout
        {
            MenuItem
            {
                text: "Save lyrics"
                onClicked:
                {
                    downloader.lyrics = lyricsview.text
                    if ( downloader.lyricsExistMaeLyrica() == true )
                    {
                        lyricsPresentMaeLyricaDialog.open()
                    }
                    else
                    {
                        downloader.saveLyricsMaeLyrica()
                        lyricsSavedBanner.show()
                    }
                }
            }
        }
    }

    QueryDialog
    {
        id: lyricsPresentMaeLyricaDialog
        acceptButtonText: "Yes"
        rejectButtonText: "No"
        titleText: "Lyrics already present"
        message: "Overwrite the lyrics in MaeLyrica database?"
        onAccepted: downloader.saveLyricsMaeLyrica()
        onRejected: saveCancelledBanner.show()
    }

    QueryDialog
    {
        id: deleteLyricsMaeLyricaDialog
        acceptButtonText: "Yes"
        rejectButtonText: "No"
        titleText: "Delete lyrics"
        message: "Delete the lyrics from MaeLyrica database?"
        onAccepted: downloader.deleteLyricsMaeLyrica()
        onRejected: lyricsDeleteCancelledBanner.show()
    }

    InfoBanner
    {
        id: saveCancelledBanner
        text: "Lyrics save cancelled"
        timerShowTime: 2000
    }
    InfoBanner
    {
        id: lyricsDeleteCancelledBanner
        text: "Lyrics deletion cancelled"
        timerShowTime: 2000
    }
    InfoBanner
    {
        id: lyricsSavedBanner
        timerShowTime: 1800
        text: "Lyrics saved successfully"
    }
}
