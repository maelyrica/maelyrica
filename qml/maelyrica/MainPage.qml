import QtQuick 1.1
import com.nokia.meego 1.0
import com.nokia.extras 1.0
import MaeLyrica 1.0

Page
{
  //  property int items:  5
   // property int itemheight: screenheight / items - screenheight
    property int items: 4
    property int heightForSpacer: (height - caption.height) / items
    property string searchString
    property bool readingSaved:  false
    id: mainPage
    tools: maintoolbar

    Loader
    {
        id: goLoader
        onLoaded: console.log("Show lyrics page loaded")
    }
    Loader
    {
        id: selectArtistLoader
        onLoaded: console.log("Select artist page loaded")
    }
    Loader
    {
        id: editTrackDataSheetLoader
        onLoaded: console.log("Edit track data sheet loaded")
    }
    Loader
    {
        id: recentSearchesSheetLoader
        onLoaded: console.log("Recent searches sheet loaded")
    }
    Loader
    {
        id: settingsSheetLoader
        onLoaded: console.log("Settings sheet loaded")
    }


    Column
    {
        width: parent.width
        MaeLyricaHeader
        {
            id: caption
            headertext: "Search for lyrics"
        }

        Item
        {
            height: heightForSpacer
            width: parent.width
            CheckBox
            {
                id: tracksrc
                text: "Select track from library" // label isn't needed with PageStackWindow
                checked: true
                anchors.centerIn: parent
            }
        }
        // button for selecting a track from the media library
        Item
        {
            height: heightForSpacer
            width: parent.width
            Row
            {
                anchors.centerIn: parent
                width: parent.width
                RowSpacer
                {
                    id: spacer1
                    width: 10 + lyricssrctext.width - selectedtracktext.width // 10 + how much wider is the 2nd text
                }

                Label
                {
                    id: selectedtracktext
                    anchors.verticalCenter: selecttrack.verticalCenter
                    text: "Selected track: "
                }

                Button
                {
                    id: selecttrack
                    text: "<i>No track selected</i>"
                    // 2 times margin - on the left and right (the same as 2nd button's left spacer)
                    width: parent.width - selectedtracktext.width - spacer1.width - spacer2.width
                    onClicked:
                    {
                        if ( tracksrc.checked  )
                        {
                            selectArtistLoader.source = "SelectArtistPage.qml"
                            pageStack.push(selectArtistLoader.item)
                        }
                        else
                        {
                            editTrackDataSheetLoader.source = "EditTrackDataSheet.qml"
                            editTrackDataSheetLoader.item.open()
                        }
                    }
                }
            }
        }

        //line edits for entering the data manually

        Item
        {
            height: heightForSpacer
            width: parent.width
            Row
            {
                anchors.centerIn: parent
                width: parent.width
                RowSpacer
                {
                    id: spacer2
                    width: 10
                }


                Label
                {
                    id: lyricssrctext
                    anchors.verticalCenter: lyricssrcbutton.verticalCenter
                    text: "Download source: "
                }

                Button
                {
                    id: lyricssrcbutton
                    text: lyricssrcdialog.model.get(lyricssrcdialog.selectedIndex).name
                    width: parent.width - 3 * spacer2.width - lyricssrctext.width
                    onClicked: { lyricssrcdialog.open(); }
                }
            }
        }

        Item
        {
            height: heightForSpacer
            width: parent.width
            Button
            {
                anchors.centerIn: parent
                id: go
                text: "Go!"
                width: parent.width
                onClicked:
                {
                    if ( downloader.anyFieldEmpty() )
                    {
                        emptyfieldsdialogmain.open()
                    }
                    else
                    {
                        goLoader.source = "ShowLyricsPage.qml" // we need to load it now so that loadSavedLyricsDialog can do its work
                        if ( downloader.theseLyricsSaved && programSettings.autoScanSaves )
                        {
                            console.log("these lyrics saved")
                            loadSavedLyricsDialog.open()
                        }
                        else
                        {
                            console.log("these lyrics not saved")
                            performDownload()
                        }

                    }
                }
            }
        }
    }
    ToolBarLayout
    {
        id: maintoolbar
        ToolIcon
        {
            iconId: "toolbar-back"
            onClicked: quitdialog.open()
        }
        ToolIcon
        {
            iconId: "toolbar-view-menu"
            onClicked: (mainMenu.status == DialogStatus.Closed) ? mainMenu.open() : mainMenu.close()
        }
    }
    Menu
    {
        id: mainMenu
        visualParent: pageStack
        MenuLayout
        {
            MenuItem
            {
                text: "Recent searches"
                onClicked:
                {
                    recentSearchesSheetLoader.source = "RecentSearchesSheet.qml"
                    recentSearchesSheetLoader.item.open()
                }
            }
            MenuItem
            {
                text: "Settings"
                onClicked:
                {
                    settingsSheetLoader.source = "SettingsSheet.qml"
                    settingsSheetLoader.item.open()
                }
            }
            MenuItem
            {
                text: "About"
                onClicked: aboutdialog.open()
            }

        }
    }

    SelectionDialog
    {
        id: lyricssrcdialog
        titleText: "Download source"
        selectedIndex: 0
        model: ListModel
        {
            ListElement { name: "AZLyrics" }
            ListElement { name: "Lyric Wiki" }
            ListElement { name: "Chart Lyrics" }
            ListElement { name: "LyricsMania" }
            ListElement { name: "Tekstowo" }
        }
        onAccepted:
        {
            switch (selectedIndex)
            {
            case 0: downloader.changeLyricsDownloader(LyricsDownloaderManager.AZLyricsDownloaderClass)
            case 1: downloader.changeLyricsDownloader(LyricsDownloaderManager.LyricWikiDownloaderClass)
            case 2: downloader.changeLyricsDownloader(LyricsDownloaderManager.ChartLyricsDownloaderClass)
            case 3: downloader.changeLyricsDownloader(LyricsDownloaderManager.LyricsManiaDownloaderClass)
            case 4: downloader.changeLyricsDownloader(LyricsDownloaderManager.TekstowoDownloaderClass)
            }
        }
    }

    QueryDialog
    {
        id: notimplementeddialog
        acceptButtonText: "OK"
        message: "Sorry! Not implemented yet!"
    }

    QueryDialog
    {
        id: emptyfieldsdialogmain
        acceptButtonText: "OK"
        titleText: "Empty fields"
        message: "Please enter data to all the fields!"
    }
    QueryDialog
    {
        id: quitdialog
        acceptButtonText: "OK"
        rejectButtonText: "Cancel"
        message: "Are you sure you want to quit MaeLyrica?"
        onAccepted: Qt.quit()
    }

    QueryDialog
    {
        id: downloadfaileddialog
        acceptButtonText: "OK"
        rejectButtonText: "Try again"
        titleText: "Download failed"
        message: "Lyrics download failed.<br />The lyrics may be absent in this provider's database"
        onRejected:
        {
            var ret = downloader.perform()
            errorDialog(ret)
        }
    }

    QueryDialog
    {
        id: lyricsnotfounddialog
        acceptButtonText: "OK"
        titleText: "Lyrics not found"
        message: "Lyrics not found. There are not present in the " + lyricssrcdialog.model.get(lyricssrcdialog.selectedIndex).name + " database."
    }

    QueryDialog
    {
        id: noInternetDialog
        acceptButtonText: "OK"
        rejectButtonText: "Try again"
        titleText: "No internet connection."
        message: "No internet connection was available.<br /> Lyrics were not downloaded"
        onRejected:
        {
            pageStack.pop()
        }
        onAccepted: pageStack.pop()
    }

    Connections
    {
        target: downloader
        onNoInternet: noInternetDialog.open()
    }

    QueryDialog
    {
        id: errorparsingdialog
        acceptButtonText: "OK"
        rejectButtonText: "Try again"
        titleText: "Error while parsing"
        message: "There was an error while parsing. The lyrics may be missing in this provider's database"
        onRejected:
        {
            var ret = downloader.perform()
            errorDialog(ret)
        }
    }

    QueryDialog
    {
        id: loadSavedLyricsDialog
        acceptButtonText: "Yes"
        rejectButtonText: "No"
        titleText: "Lyrics already saved"
        message: "Lyrics for this track have already been saved. Do you want them to be shown?"
        onAccepted:
        {
            readingSaved = true
            pageStack.push(goLoader.item)
        }
        onRejected:
        {
            performDownload()
        }
    }

    QueryDialog
    {
        id: aboutdialog
        titleText: "MaeLyrica 1.0"
        message: "<br /> Copyright (C) 2012 Marcin Mielniczuk <br /><br /> If you want to support my work, please consider <a href='https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=WUAUBSU3QQLNL'> donating </a> "
    }

    function performDownload()
    {
        programSettings.addRecentSearch(selecttrack.text)
        var rv = downloader.perform() // return value from lyricsDownloader::perform()
        errorDialog(rv)
    }

    function errorDialog(r)
    {
        if ( r == 1)
        {
            downloadfaileddialog.open()
        }
        else if ( r == 2 )
        {
            errorparsingdialog.open()
        }
        else if (r == 3)
        {
            lyricsnotfounddialog.open()
        }
        else
        {
            pageStack.push(goLoader.item)
        }
    }
}

