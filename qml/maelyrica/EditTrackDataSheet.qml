import QtQuick 1.1
import com.nokia.meego 1.0

Sheet {
    id: editTrackDataSheet

    acceptButtonText: "OK"
    rejectButtonText: "Cancel"
    onAccepted:
    {
        if ( artistfield.text == "" || trackfield.text == "" ) // check whether empty
        {
            emptyfieldsdialog.open()
        }
        else
        {
            selecttrack.text = artistfield.text + " - " + trackfield.text
            downloader.setData(artistfield.text, trackfield.text)
            searchString = selecttrack.text
        }
    }

    content: Rectangle
    {
        anchors.fill: parent
        width: parent.width
        height: parent.height
        color: "#E0E1E2"
        Column
        {
            anchors.top: parent.top
            anchors.topMargin: 20
            spacing: 20
            width: parent.width
            TextField
            {
                id: artistfield
                placeholderText: "Artist"
                width: parent.width
            }
            TextField
            {
                id: trackfield
                placeholderText: "Track"
                width: parent.width
            }
        }
    }

    QueryDialog
    {
        id: emptyfieldsdialog
        acceptButtonText: "OK"
        titleText: "Empty fields"
        message: "Please enter data to all the fields!"
        onAccepted: editTrackDataSheet.open()
        onRejected: editTrackDataSheet.open()
    }
}

