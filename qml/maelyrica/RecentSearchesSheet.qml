import QtQuick 1.1
import com.nokia.meego 1.0

Sheet
{
    id: recentSearchesSheet
    rejectButtonText: "Cancel"
    content: Rectangle
    {
        anchors.fill: parent
        width: parent.width
        height: parent.height
        color: "#E0E1E2"
        ListView
        {
            anchors.fill: parent
            clip: true
            model: programSettings.recentSearches
            visible: programSettings.recentSearches.length > 0
            delegate: Item
            {
                height: 60
                width: parent.width
                Button
                {
                    text: modelData
                    width: parent.width
                    height: 50
                    font.pixelSize: height / 2
                    anchors.verticalCenter: parent.verticalCenter

                    onClicked:
                    {
                        downloader.setData(modelData)
                        selecttrack.text = modelData
                        searchString = modelData
                        recentSearchesSheet.close()
                    }
                }
            }
        }
        Text
        {
            color: "darkgray"
            visible: programSettings.recentSearches.length <= 0
            text: "No recent searches"
            anchors.centerIn: parent
            font.pointSize: 40
        }
    }
}
