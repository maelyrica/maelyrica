import QtQuick 1.1
import com.nokia.meego 1.0
import QtMobility.gallery 1.1

Page
{
    id: selectArtistPage
    tools: backtoolbar
    property string selectedartist

    Loader
    {
        id: selectAlbumPageLoader
        onLoaded: console.log("Select album page loaded")
    }

    ListView
    {
        id: galleryView
        anchors.fill: parent
        clip: true
        model: artistModel
        delegate: Item
        {
            height: 60
            width: parent.width
            Button
            {
                height: 50
                width: parent.width - 20
                text: (artist != "") ? artist : "(unknown artist)"
                font.pixelSize: height / 2

                anchors
                {
                    verticalCenter: parent.verticalCenter
                    left: parent.left
                    leftMargin: 10
                }
                onClicked:
                {
                    selectedartist = (text != "(unknown artist)") ? text : ""
                    selectAlbumPageLoader.source = "SelectTrackPage.qml"
                    pageStack.replace(selectAlbumPageLoader.item)
                }
            }
            Image
            {
                source: "image://theme/icon-m-common-drilldown-arrow"
                anchors
                {
                    right: parent.right
                    rightMargin: 15
                    verticalCenter: parent.verticalCenter
                }
            }

        }
        header: MaeLyricaHeader {headertext: "Select artist"}
    }

    DocumentGalleryModel
    {
        id: artistModel
        rootType: DocumentGallery.Artist
        properties: ["artist"]
        sortProperties: ["+artist"]
    }
    //}
    ToolBarLayout
    {
        id: backtoolbar
        ToolIcon
        {
            iconId: "toolbar-back"
            onClicked: pageStack.pop()
        }
    }
}
