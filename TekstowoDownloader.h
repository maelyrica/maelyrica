#ifndef TEKSTOWODOWNLOADER_H
#define TEKSTOWODOWNLOADER_H

#include "lyricsDownloader.h"

class TekstowoDownloader : public lyricsDownloader
{
protected:
    virtual QString toProviderCode(const QString &artist, const QString &track) const;
    virtual bool parse();
};

#endif // TEKSTOWODOWNLOADER_H
