#include "LyricWikiDownloader.h"
#include <QTextDocument>
#include <QDebug>

const QString LyricWikiDownloader::rooturl("http://lyrics.wikia.com");

QString LyricWikiDownloader::toProviderCode(const QString &artist, const QString &track) const
{
    QString artist_www, track_www;
    artist_www = artist.trimmed().replace(' ', '_');
    track_www = track.trimmed().replace(' ', '_');
    return (QString(rooturl + "/%1:%2").arg(artist_www, track_www));
}

bool LyricWikiDownloader::parse()
{
    QString startMarker("<div class='lyricbox\'>");
    int startpos = lyrics_qstr.indexOf(startMarker) + startMarker.length();
    if (startpos < 0) return false;

    lyrics_qstr = lyrics_qstr.remove(0, startpos);
    lyrics_qstr = QTextDocument(lyrics_qstr.mid(lyrics_qstr.indexOf("</div>") + 6, lyrics_qstr.indexOf("<!--"))).toPlainText();

    return true;

    /*istringstream temp(buff.str());
    buff.str("");
    string line;
    QString line_QStr;
    while (temp.good())
    {
        getline(temp, line);
        if ((line_QStr = QString::fromUtf8(line.c_str())).contains("<div class='lyricbox\'>"))
        {
            int lyricsstart = line_QStr.indexOf("</div>") + 6;
            int lyricslength = line_QStr.indexOf("<!-- ") - lyricsstart;
            lyrics_qstr = line_QStr.mid(lyricsstart, lyricslength);
            break;
        }
    }
    if ( !temp.good() ) return false; // something went wrong

    QTextDocument lyricsHtml;
    lyricsHtml.setHtml(lyrics_qstr);
    lyrics_qstr = lyricsHtml.toPlainText();
    return true;*/
}
