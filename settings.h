/* File: main.cpp
 *
 * This file is part of MaeLyrica.
 *
 * Copyright (C) 2012 Marcin Mielniczuk.
 *
 * MaeLyrica is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 * MaeLyrica is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with MaeLyrica.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SETTINGS_H
#define SETTINGS_H

#include <QSettings>
#include <QStringList>

class Settings : public QSettings
{
    Q_OBJECT
    Q_PROPERTY(QStringList recentSearches READ getRecentSearches WRITE setRecentSearches NOTIFY recentSearchesChanged )
    Q_PROPERTY(bool anyRecentSearches READ getAnyRecentSearches NOTIFY recentSearchesChanged)
    Q_PROPERTY(bool autoScanSaves READ getAutoReadSaves WRITE setAutoReadSaves NOTIFY autoScanSavesChanged)

public:
    explicit Settings(QObject * parent = 0) : QSettings( "Marcin Mielniczuk", "MaeLyrica", parent) {}
    Q_INVOKABLE void restoreDefaults();
    Q_INVOKABLE void addRecentSearch(QString search);
    Q_INVOKABLE inline void clearSavedLyrics() { remove("lyrics"); }
signals:
    void recentSearchesChanged(QStringList);
    void autoScanSavesChanged(bool);
protected:
    inline void setAutoReadSaves(bool whether)
        {setValue("saves/autoScan", whether); emit autoScanSavesChanged(whether);}
    inline void setRecentSearches(QStringList searches = QStringList())
        {setValue("recent/searches", searches); sync(); emit recentSearchesChanged(searches);}
    inline bool getAutoReadSaves() {return value("saves/autoScan", true).toBool();}
    inline QStringList getRecentSearches() {return value("recent/searches").value<QStringList>();}
    bool getAnyRecentSearches()
    {
        if (getRecentSearches() == QStringList()) return false; // no recent searches
        else return true;
    }
};

#endif // SETTINGS_H
