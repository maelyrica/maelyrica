/* File: lyricsDownloader.cpp
 *
 * This file is part of MaeLyrica.
 *
 * Copyright (C) 2012 Marcin Mielniczuk.
 *
 * MaeLyrica is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 * MaeLyrica is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with MaeLyrica.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "lyricsDownloader.h"

#include <curl/curl.h>
#include <QDebug>
#include <iostream>
#include <QFile>
#include <QSettings>
#include <QtNetwork/QtNetwork>
#include <iostream>

// include the downloaders here
#include "LyricWikiDownloader.h"
#include "AZLyricsDownloader.h"
#include "ChartLyricsDownloader.h"
#include "LyricsManiaDownloader.h"
#include "TekstowoDownloader.h"

/* How to add new downloader:
 * 1) Add new value to LyricsDownloaderManager::LyricsDownloaderClass
 * 2) Add new case to LyricsDownloaderManager::newDownloader
 * 3) Add new entry to the SelectionDialog in MainPage.qml
 */

using namespace std;

/////////////////////////////////////////////////////////////////////////////

short lyricsDownloader::perform(const QString& a, const QString& t)
{
    QNetworkReply::NetworkError dlret; // return value
    dlret = download(a, t); // it doesn't fail yet to recognize

    if (dlret == 50) return -1; // no internet
    else if (dlret != 0) return 1; // error downloading
    //else continues!

    if ( parse() == false)
    {
        qDebug() << "Error while parsing lyrics\n";
        return 2;
    }

    return 0; // ok
}

QNetworkReply::NetworkError lyricsDownloader::download(const QString& a, const QString& t)
{
    /**/

    QNetworkAccessManager nam;
    QNetworkReply * reply;
    QUrl url = toProviderCode(a, t);
    forever
    {
        reply = nam.get(QNetworkRequest(url));
        QEventLoop downloadLoop;
        connect(reply, SIGNAL(finished()), &downloadLoop, SLOT(quit()));
        downloadLoop.exec();
        url = reply->attribute(QNetworkRequest::RedirectionTargetAttribute).toUrl();
        if (url.isEmpty()) break; // break when no redirect, when redirect do it again with new url
    }

    lyrics_qstr = reply->readAll();

    //qDebug() << lyrics_qstr;

    return reply->error();

    /*CURL * handle;
    CURLcode err;
    handle = curl_easy_init();
    if (! handle) return static_cast<CURLcode>(-1);
    // set verbose if debug on
    curl_easy_setopt( handle, CURLOPT_VERBOSE, DEBUG );
    curl_easy_setopt( handle, CURLOPT_URL, toProviderCode(a, t).c_str() ); // set the download url to the generated one
    curl_easy_setopt(handle, CURLOPT_WRITEDATA, &buff);
    curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, &lyricsDownloader::write_data_to_var); // set the function writing to ostringstream
    curl_easy_setopt(handle, CURLOPT_FOLLOWLOCATION, 1); // curl needs to follow redirects with this provider
    curl_easy_setopt(handle, CURLOPT_FAILONERROR, 1);
    curl_easy_setopt(handle, CURLOPT_ENCODING, "UTF-8");
    if (manager.isOnline() == false) return static_cast<CURLcode>(-2);
    err = curl_easy_perform(handle);
    curl_easy_cleanup(handle);
    return err;*/
}

/////////////////////////////////////////////////////////////////////////

Settings LyricsDownloaderManager::progSettings;

LyricsDownloaderManager::LyricsDownloaderManager(LyricsDownloaderClass lyricsDownloaderName)
{
    connect(this, SIGNAL(dataChanged()), this, SLOT(cleanTitle()));
    connect(this, SIGNAL(dataChanged()), this, SLOT(findSavedLyricsKey())); // recalculate the key, so that it's always up-to-date

    currentClass = lyricsDownloaderName;
    downloader = newDownloader(lyricsDownloaderName);
}

LyricsDownloaderManager::~LyricsDownloaderManager()
{
    delete downloader;
}

void LyricsDownloaderManager::changeLyricsDownloader(LyricsDownloaderClass lyricsDownloaderName)
{
    qDebug() << "Changing lyricsDownloader to " << lyricsDownloaderName << "\n";
    if (lyricsDownloaderName != currentClass) // we don't need to do the same again if it's the same downloader
    {
        lyricsDownloader * oldDownloader = downloader;
        downloader = newDownloader(lyricsDownloaderName);
        delete oldDownloader;
    }
}

lyricsDownloader * LyricsDownloaderManager::newDownloader(LyricsDownloaderClass & name)
{
    switch (name)
    {
    case AZLyricsDownloaderClass:
        return new AZLyricsDownloader;
        break;
    case LyricWikiDownloaderClass:
        return new LyricWikiDownloader;
        break;
    case ChartLyricsDownloaderClass:
        return new ChartLyricsDownloader;
        break;
    case LyricsManiaDownloaderClass:
        return new LyricsManiaDownloader;
        break;
    case TekstowoDownloaderClass:
        return new TekstowoDownloader;
    }
    return 0; // in case of error NULL returned
}

short LyricsDownloaderManager::perform()
{
    QNetworkConfigurationManager manager;
    QNetworkConfiguration cfg = manager.defaultConfiguration();
    /*QNetworkSession* session = new QNetworkSession(cfg);
    session->setSessionProperty("ConnectInBackground", true);
    session->open();*/
    if (manager.isOnline() == false)
    {
        emit noInternet();
        return -1;
    }

    short ret = downloader -> perform(artist, track);
    lyrics = downloader -> lyrics_qstr; // clean it too!
    downloader -> lyrics_qstr.clear();
    emit lyricsChanged(lyrics);
    return ret;
}

void LyricsDownloaderManager::cleanTitle()
{
    cleanedTitle = cleanQString(artist);
    cleanedTitle += "-";
    cleanedTitle += cleanQString(track);
}

QString LyricsDownloaderManager::cleanQString(const QString qstr)
{
    return qstr.toLower().replace("&", "and").remove(QRegExp("\\([^)]*\\)")).remove(QRegExp("[\\W_]"));
}

void LyricsDownloaderManager::saveLyricsMaeLyrica()
{
    Settings().setValue("lyrics/" + cleanedTitle, lyrics);
}
