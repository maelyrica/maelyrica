#ifndef AZLYRICSDOWNLOADER_H
#define AZLYRICSDOWNLOADER_H

#include <QDebug>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkAccessManager>

#include "lyricsDownloader.h"

using namespace std;

class AZLyricsDownloader : public lyricsDownloader
{
    Q_OBJECT
public:
    //AZLyricsDownloader() : lyricsDownloader("", "") {}
    //AZLyricsDownloader(const string & a, const string & t) : lyricsDownloader(a, t) {}
    //virtual short perform(string a, string t);

protected:

    //virtual QNetworkReply::NetworkError download(const QString& a, const QString& t);
    virtual QString toProviderCode(const QString& artist, const QString& track) const;
    virtual bool parse();
    /**/
    static const QString rooturl, startcomment, endcomment; // comments in the azlyrics.com website which indicate start and end of lyrics
};
#endif // AZLYRICSDOWNLOADER_H
