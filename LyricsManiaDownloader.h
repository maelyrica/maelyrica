#ifndef LYRICSMANIADOWNLOADER_H
#define LYRICSMANIADOWNLOADER_H

#include "lyricsDownloader.h"

class LyricsManiaDownloader : public lyricsDownloader
{
protected:
    virtual QString toProviderCode(const QString &artist, const QString &track) const;
    virtual bool parse();
    static const QString rooturl, startMarker, endMarker;

};

#endif // LYRICSMANIADOWNLOADER_H
