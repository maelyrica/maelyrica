/* File: main.cpp
 *
 * This file is part of MaeLyrica.
 *
 * Copyright (C) 2012 Marcin Mielniczuk.
 *
 * MaeLyrica is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 * MaeLyrica is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with MaeLyrica.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QtGui/QApplication>
#include <QString>
#include <QDebug>
#include <QTextEdit>
#include <iostream>
#include <QtDeclarative>

#include "qmlapplicationviewer.h"
#include "lyricsDownloader.h"
#include "AZLyricsDownloader.h"
#include "settings.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QmlApplicationViewer viewer;

    QNetworkConfigurationManager manager;
    QNetworkConfiguration cfg = manager.defaultConfiguration();
    QNetworkSession* session = new QNetworkSession(cfg);
    session->setSessionProperty("ConnectInBackground", true);
    session->open();

    // qml preparations

#ifdef Q_WS_MAEMO_5
    viewer.engine()->addImportPath(QString("/opt/qtm12/imports"));
    viewer.engine()->addPluginPath(QString("/opt/qtm12/plugins"));
#endif
    qmlRegisterUncreatableType<lyricsDownloader>("MaeLyrica", 1, 0, "lyricsDownloader", "");
    qmlRegisterType<AZLyricsDownloader>("MaeLyrica", 1, 0, "AZLyricsDownloader");
    qmlRegisterType<Settings>("MaeLyrica", 1, 0, "Settings");
    qmlRegisterType<LyricsDownloaderManager>("MaeLyrica", 1, 0, "LyricsDownloaderManager");

    viewer.setOrientation(QmlApplicationViewer::ScreenOrientationAuto);
    viewer.setMainQmlFile(QLatin1String("qml/maelyrica/main.qml"));
    viewer.showFullScreen();

    return app.exec();

}
