#ifndef LYRICWIKIDOWNLOADER_H
#define LYRICWIKIDOWNLOADER_H

#include "lyricsDownloader.h"

class LyricWikiDownloader : public lyricsDownloader
{
public:
protected:
    virtual QString toProviderCode(const QString &artist, const QString &track) const;
    virtual bool parse();
    static const QString rooturl;
};

#endif // LYRICWIKIDOWNLOADER_H
