#include "LyricsManiaDownloader.h"
#include <QTextDocument>

const QString LyricsManiaDownloader::rooturl("http://www.lyricsmania.com/%1_lyrics_%2.html");
const QString LyricsManiaDownloader::startMarker("<div id='songlyrics_h' class='dn'>");
const QString LyricsManiaDownloader::endMarker("</div>");

QString LyricsManiaDownloader::toProviderCode(const QString &artist, const QString &track) const
{
    QString artist_www = artist.trimmed().toLower().replace(' ', '_');
    QString track_www = track.trimmed().toLower().replace(' ', '_').replace('\'','_');
    return rooturl.arg(track_www, artist_www);
}

bool LyricsManiaDownloader::parse()
{
    int startpos = lyrics_qstr.indexOf(startMarker) + startMarker.length();
    if (startpos < 0) return false;

    lyrics_qstr = QTextDocument(lyrics_qstr.remove(0, startpos).mid(0, lyrics_qstr.indexOf(endMarker))).toPlainText();

    return true;

    /*istringstream temp(buff.str());
    buff.str("");
    string line;
    QString line_QStr, lyrics_QStr;
    while (temp.good())
    {
        getline(temp, line);
        if (QString::fromUtf8(line.c_str()).contains(startMarker)) break; // we don't need utf8 yet
    }
    if (!temp.good()) return false; // something went wrong

    while (temp.good())
    {
        getline(temp, line);
        if ((line_QStr = QString::fromUtf8(line.c_str())).contains(endMarker))
        {
            lyrics_QStr += line_QStr.remove(endMarker); // remove the </div>
            break;
        }
        else
        {
            lyrics_QStr += line_QStr;
        }
    }

    if (!temp.good()) return false;

    QTextDocument lyricsHtml;
    lyricsHtml.setHtml(lyrics_QStr);
    lyrics_qstr = lyricsHtml.toPlainText();*/
}
