#include "AZLyricsDownloader.h"
#include <QTextDocument>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QUrl>
#include <QEventLoop>

const QString AZLyricsDownloader::rooturl("http://www.azlyrics.com/lyrics");
const QString AZLyricsDownloader::endcomment("<!-- end of lyrics -->");
const QString AZLyricsDownloader::startcomment("<!-- start of lyrics -->");

QString AZLyricsDownloader::toProviderCode(const QString& artist, const QString& track) const
{
    QString artist_www, track_www;
    artist_www = artist.toLower().remove(QRegExp("^the")).remove(QRegExp("\\W"));
    track_www = track.toLower().remove(QRegExp("\\W"));

    return (rooturl + "/" + artist_www + "/" + track_www + ".html");

}

bool AZLyricsDownloader::parse()
{
    int startpos = lyrics_qstr.indexOf(startcomment) + startcomment.length();
    int endpos = lyrics_qstr.indexOf(endcomment);

    if ((startpos < 0) || (endpos < 0) || startpos > endpos) return false;
    lyrics_qstr = QTextDocument(lyrics_qstr.mid(startpos, lyrics_qstr.indexOf(endcomment) - startpos) ).toPlainText();

    return true;
}

