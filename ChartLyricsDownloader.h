#ifndef CHARTLYRICSDOWNLOADER_H
#define CHARTLYRICSDOWNLOADER_H

#include "lyricsDownloader.h"

class ChartLyricsDownloader : public lyricsDownloader
{
protected:
    virtual QString toProviderCode(const QString &artist, const QString &track) const;
    virtual bool parse();
    static const QString rooturl;
};

#endif // CHARTLYRICSDOWNLOADER_H
