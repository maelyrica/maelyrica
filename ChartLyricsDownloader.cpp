#include "ChartLyricsDownloader.h"
#include <QtXml/QXmlStreamReader>
const QString ChartLyricsDownloader::rooturl("http://api.chartlyrics.com/apiv1.asmx/SearchLyricDirect");

QString ChartLyricsDownloader::toProviderCode(const QString &artist, const QString &track) const
{
    QUrl rootqurl(rooturl);
    rootqurl.addQueryItem("artist", artist.trimmed());
    rootqurl.addQueryItem("song", track.trimmed());
    return rootqurl.toString() ;
}

bool ChartLyricsDownloader::parse()
{
    QXmlStreamReader reader(lyrics_qstr);
    lyrics_qstr = "";
    while ( !reader.atEnd() && !reader.hasError())
    {
        if (reader.readNext() == QXmlStreamReader::StartElement && reader.name() == "Lyric")
            lyrics_qstr = reader.readElementText();
    }

    if (lyrics_qstr.isEmpty()) return false;
    else return true;
}
